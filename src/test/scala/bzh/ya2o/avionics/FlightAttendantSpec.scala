package bzh.ya2o.avionics

import bzh.ya2o.avionics.FlightAttendant.{Drink, GetDrink}
import akka.testkit.{TestActorRef, ImplicitSender, TestKit}
import akka.actor.{Props, ActorSystem}
import org.scalatest.{Matchers, WordSpecLike}
import com.typesafe.config.ConfigFactory

object TestFlightAttendant {
  def apply() = new FlightAttendant with AttendantResponsiveness {
    override val maxResponseTimeMS = 1 // Fast, in order to make test run fast`
  }
}

class FlightAttendantSpec
  extends TestKit(ActorSystem("FlightAttendantSpec", ConfigFactory.parseString("akka.scheduler.tick-duration = 1 ms"))) // 1ms instead of default 100ms, to make tests quickier
          with ImplicitSender with WordSpecLike with Matchers {

  "FlightAttendant" should {
    "get a drink when asked" in {
      val a = TestActorRef(Props(TestFlightAttendant()))
      a ! GetDrink("Soda")
      expectMsg(Drink("Soda"))
    }
  }
}
