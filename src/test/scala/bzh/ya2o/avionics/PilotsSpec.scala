package bzh.ya2o.avionics

import akka.actor._
import akka.pattern.ask
import akka.testkit.{TestKit, ImplicitSender, TestProbe}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.scalatest.{Matchers, WordSpecLike}
import scala.concurrent.Await
import scala.concurrent.duration._

class FakePilot extends Actor {
  override def receive = {
    case _ =>
  }
}

object PilotsSpec {
  val copilotName = "Mary"
  val pilotName = "Mark"
  val configStr = s"""
    bzh.ya2o.avionics.flightCrew.copilotName = "$copilotName"
    bzh.ya2o.avionics.flightCrew.pilotName = "$pilotName""""
}

class PilotsSpec
        extends TestKit(ActorSystem("PilotsSpec",
                                     ConfigFactory.parseString(PilotsSpec.configStr)))
                with ImplicitSender with WordSpecLike with Matchers {

  import PilotsSpec._

  import Plane._

  def nilActor: ActorRef = TestProbe().ref

  val pilotPath = s"/user/TestPilots/$pilotName"
  val copilotPath = s"/user/TestPilots/$copilotName"

  def pilotsReadyToGo(): ActorRef = {
    implicit val askTimeout = Timeout(4.seconds)

    val a = system.actorOf(
                            Props(new IsolatedStopSupervisor with OneForOneStrategyFactory {
                              def childStarter() {
                                context.actorOf(Props[FakePilot], pilotName)
                                context.actorOf(Props(new Copilot(testActor, nilActor)), copilotName)
                              }
                            }),
                            "TestPilots")
    Await.result(a ? IsolatedLifeCycleSupervisor.WaitForStart, 3.seconds)
    system.actorFor(copilotPath) ! Pilot.ReadyToGo
    a
  }

  "CoPilot" should {
    "take control when the Pilot dies" in {
      pilotsReadyToGo()
      // kill the pilot
      system.actorFor(pilotPath) ! PoisonPill
      expectMsg(GiveMeControl)
      lastSender should be(system.actorFor(copilotPath))
    }
  }
}
