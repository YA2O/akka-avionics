package bzh.ya2o.avionics

import akka.actor.{Props, Actor, ActorSystem}
import akka.testkit.{TestKit, TestActorRef, TestLatch, ImplicitSender}
import scala.concurrent.duration._
import scala.concurrent.Await
import org.scalatest.{Matchers, WordSpecLike, BeforeAndAfterAll}

class AltimeterSpec extends TestKit(ActorSystem("AltimeterSpec"))
                            with ImplicitSender
                            with WordSpecLike
                            with Matchers
                            with BeforeAndAfterAll {
  import Altimeter._

  override def afterAll() { system.shutdown() }

  // we'll instantiate a Helper class for every test, to make things reusable
  class Helper {
    // the latch gives us fast feedback when something happens
    object EventSourceSpy {
      val latch = TestLatch(1)
    }

    // our special derivation of EventSource gives us the hooks into concurrency
    trait EventSourceSpy extends EventSource {
      def sendEvent[T](event: T): Unit = EventSourceSpy.latch.countDown()

      def eventSourceReceive = Actor.emptyBehavior
    }

    def slicedAltimeter = new Altimeter with EventSourceSpy

    def actor() = {
      val a = TestActorRef[Altimeter](Props(slicedAltimeter))
      (a, a.underlyingActor)
    }
  }


  "Altimeter" should {

    "record rate of climb changes" in new Helper {
      val (_, real) = actor()
      real.receive(RateChange(1f))
      real.rateOfClimb should be(real.maxRateOfClimb)
    }

    "keep rate of climb changes within bounds" in new Helper {
      val (_, real) = actor()
      real.receive(RateChange(2f))
      real.rateOfClimb should be(real.maxRateOfClimb)
    }

    "calculate altitude changes" in new Helper {
      val ref = system.actorOf(Props(Altimeter()))
      ref ! EventSource.RegisterListener(testActor)
      ref ! RateChange(1f)
      fishForMessage() {
        case AltitudeUpdate(altitude) if altitude == 0f =>
          false
        case AltitudeUpdate(altitude) =>
          true
      }
    }

    "send events" in new Helper {
      val (ref, _) = actor()
      Await.ready(EventSourceSpy.latch, 1.second)
      EventSourceSpy.latch.isOpen should be(true)
    }
  }
}


