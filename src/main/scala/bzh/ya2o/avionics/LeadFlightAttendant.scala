package bzh.ya2o.avionics

import akka.actor.{Props, Actor, ActorRef}

trait AttendantCreationPolicy {
  val numberOfAttendants: Int = 8

  def createAttendant: Actor = FlightAttendant()
}

trait LeadFlightAttendantProvider {
  def newLeadFlightAttendant: Actor = LeadFlightAttendant()
}

object LeadFlightAttendant {

  case object GetFlightAttendant

  case class Attendant(a: ActorRef)

  def apply() = new LeadFlightAttendant with AttendantCreationPolicy
}

class LeadFlightAttendant extends Actor {this: AttendantCreationPolicy =>

  import LeadFlightAttendant._


  override def preStart(): Unit = {
    import scala.collection.JavaConverters._

    val attendantNames = context.system.settings.config.getStringList("bzh.ya2o.avionics.flightCrew.attendantNames").asScala

    attendantNames take numberOfAttendants foreach {
      name =>
        context.actorOf(Props(createAttendant), name)
    }

  }

  def randomAttendant(): ActorRef = {
    context.children.take(scala.util.Random.nextInt(numberOfAttendants) - 1).last
  }

  override def receive: Receive = {
    case GetFlightAttendant =>
      sender ! Attendant(randomAttendant())
    case m =>
      randomAttendant() forward m
  }
}

// short program to check the actor hierarchy
object FlightAttendantPathChecker {

  import akka.actor.ActorSystem

  def main(args: Array[String]) {
    val system = ActorSystem("PlaneSimulation")
    val lead = system.actorOf(
      Props(new LeadFlightAttendant with AttendantCreationPolicy),
      system.settings.config.getString("bzh.ya2o.avionics.flightCrew.leadAttendantName"))
    Thread.sleep(2000)
    system.shutdown()
  }
}
