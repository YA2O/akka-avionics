package bzh.ya2o.avionics

import akka.actor.{Actor, ActorRef}

// this object carries messages for controlling the plane
object ControlSurfaces {
  //amount is a value between -1 and 1 (should be positive ???)
  case class StickBack(amount: Float)
  case class StickForward(amount: Float)
}

class ControlSurfaces(altimeter: ActorRef) extends Actor {
  import ControlSurfaces._
  import Altimeter._

  def receive = {
    case StickBack(amount) => altimeter ! RateChange(amount)
    case StickForward(amount) => altimeter ! RateChange(-1 * amount)
  }

}
