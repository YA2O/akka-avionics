package bzh.ya2o.avionics

import akka.actor._
import akka.util.Timeout
import bzh.ya2o.avionics.IsolatedLifeCycleSupervisor.WaitForStart

object Plane {

  // returns the control surface to the actor asking for them
  case object GiveMeControl

  case class Controls(controls: ActorRef)

  def apply() = new Plane with AltimeterProvider with PilotProvider with LeadFlightAttendantProvider

}

class Plane extends Actor with ActorLogging {this: AltimeterProvider with PilotProvider with
        LeadFlightAttendantProvider =>

  import Plane._
  import Pilot._
  import Altimeter._
  import EventSource._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  import akka.pattern.ask

  val config = context.system.settings.config
  val configStr = "bzh.ya2o.avionics.flightCrew"

  val pilotName = config.getString(s"$configStr.pilotName")
  val copilotName = config.getString(s"$configStr.copilotName")
  val leadFlightAttendantName = config.getString(s"$configStr.leadAttendantName")

  def actorForControls(name: String) = context.actorFor("Equipment/" + name)

  def actorForPilots(name: String) = context.actorFor("Pilots/" + name)

  override def preStart(): Unit = {
    // order is important here
    startEquipment()
    startPeople()
    // bootstrap the system
    actorForControls("Altimeter") ! RegisterListener(self)
    actorForControls(pilotName) ! ReadyToGo
    actorForControls(copilotName) ! ReadyToGo
  }

  def receive = {
    case GiveMeControl            =>
      log.info(s"Plane giving control")
      sender ! actorForControls("ControlSurfaces")
    case AltitudeUpdate(altitude) =>
      log info s"Altitude is now: $altitude"
  }

  implicit val askTimeout = Timeout(1.second) // needed for the ?-syntax

  def startEquipment() {
    val controls = context.actorOf(Props(
                                          new IsolatedResumeSupervisor with OneForOneStrategyFactory {
                                            def childStarter() {
                                              // these children get implicitely added to the hierarchy
                                              val alt = context.actorOf(Props(newAltimeter), "Altimeter")
                                              //          context.actorOf(Props(newAutopilot), "AutoPilot")
                                              context.actorOf(Props(new ControlSurfaces(alt)), "ControlSurfaces")
                                            }
                                          }
                                        ),
                                    "Equipment")
    Await.result(controls ? WaitForStart, 1.second)
  }

  def startPeople() {
    val plane = self
    val controls = actorForControls("ControlSurfaces")
    //    val autopilot = actorForControls("Autopilot")
    val altimeter = actorForControls("Altimeter")
    val people = context.actorOf(Props(
                                        new IsolatedStopSupervisor with OneForOneStrategyFactory {
                                          def childStarter() {
                                            context.actorOf(Props(newPilot(plane, controls, altimeter)), pilotName)
                                            context.actorOf(Props(newCopilot(plane, controls, altimeter)), copilotName)
                                          }
                                        }
                                      ),
                                  "Pilots")
    // Use the default strategy here, which restarts indefinitely
    context.actorOf(Props(newLeadFlightAttendant), leadFlightAttendantName)
    Await.result(people ? WaitForStart, 1.second)
  }

}
