package bzh.ya2o.avionics

import akka.actor._
import akka.actor.SupervisorStrategy._
import concurrent.duration.Duration

object IsolatedLifeCycleSupervisor {

  // messages we use in case we want people to be able to wait for us to finish starting
  case object WaitForStart

  case object Started

}

trait IsolatedLifeCycleSupervisor extends Actor {

  import IsolatedLifeCycleSupervisor._

  def receive = {
    case WaitForStart =>
      sender ! Started
    case m            =>
      throw new Exception(s"Don't call ${ self.path.name } directly ($m).")
  }

  //to be implemented by subclass
  def childStarter(): Unit

  //start children when we're started
  final override def preStart() {
    childStarter()
  }

  //override default preStart() call
  final override def postRestart(reason: Throwable) {}

  //override default children stop
  final override def preRestart(reason: Throwable, message: Option[Any]) {}

}

abstract class IsolatedResumeSupervisor(maxNrRetries: Int = -1, withinTimeRange: Duration = Duration.Inf)
        extends IsolatedLifeCycleSupervisor {this: SupervisionStrategyFactory =>
  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange) {
    case _: ActorInitializationException => Stop
    case _: ActorKilledException         => Stop
    case _: Exception                    => Resume
    case _                               => Escalate
  }
}

abstract class IsolatedStopSupervisor(maxNrRetries: Int = -1, withinTimeRange: Duration = Duration.Inf)
        extends IsolatedLifeCycleSupervisor {this: SupervisionStrategyFactory =>
  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange) {
    case _: ActorInitializationException => Stop
    case _: ActorKilledException         => Stop
    case _: Exception                    => Stop
    case _                               => Escalate
  }
}



