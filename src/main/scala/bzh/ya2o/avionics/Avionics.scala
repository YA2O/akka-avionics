package bzh.ya2o.avionics

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._

// execution context used by the futures created by the ask syntax
import scala.concurrent.ExecutionContext.Implicits.global

object Avionics {
  // needed for '?' below
  implicit val timeout =  Timeout(5.seconds)

  val system  =  ActorSystem("PlaneSimulation")
  val plane  =  system.actorOf(Props(Plane()),"Plane")


  def main(args: Array[String]){
    val control = Await.result(
      (plane ? Plane.GiveMeControl).mapTo[ActorRef],
      5.seconds)

    // take off
    system.scheduler.scheduleOnce(200.millis){
      control ! ControlSurfaces.StickBack(1f)
    }
    // level out
    system.scheduler.scheduleOnce(1.seconds) {
      control ! ControlSurfaces.StickBack(0f)
    }
    // climb
    system.scheduler.scheduleOnce(3.seconds) {
      control ! ControlSurfaces.StickBack(.5f)
    }
    // level out
    system.scheduler.scheduleOnce(4.seconds) {
      control ! ControlSurfaces.StickBack(0f)
    }
    // shut down
    system.scheduler.scheduleOnce(5.seconds) {
      system.shutdown()
    }
  }

}
