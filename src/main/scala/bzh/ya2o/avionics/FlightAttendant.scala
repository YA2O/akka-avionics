package bzh.ya2o.avionics

import scala.concurrent.duration._
import akka.actor.Actor

trait AttendantResponsiveness {
  val maxResponseTimeMS: Int

  def responseDuration = scala.util.Random.nextInt(maxResponseTimeMS).millis
}

object FlightAttendant {

  case class GetDrink(drinkName: String)

  case class Drink(drinkName: String)

  def apply() = new FlightAttendant with AttendantResponsiveness {
    override val maxResponseTimeMS: Int = 300000 // respond within 5 minutes
  }
}

class FlightAttendant extends Actor {this: AttendantResponsiveness =>
  import FlightAttendant._

  implicit val executionContext = context.dispatcher // needed for the scheduler

  override def receive: Receive = {
    case GetDrink(drinkName) =>
      context.system.scheduler.scheduleOnce(responseDuration, sender, Drink(drinkName))
  }
}
