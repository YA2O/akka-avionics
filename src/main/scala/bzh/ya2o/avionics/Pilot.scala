package bzh.ya2o.avionics

import akka.actor.{Terminated, ActorRef, Actor}

object Pilot {

  case object ReadyToGo

  case object RelinquishControl

}

class Pilot(plane: ActorRef, var controls: ActorRef, altimeter: ActorRef) extends Actor {

  import Pilot._
  import Plane._

  //  var controls: ActorRef = context.system.deadLetters
  var copilot: ActorRef = context.system.deadLetters
  //  var autopilot: ActorRef = context.system.deadLetters
  var copilotName = context.system.settings.config.getString("bzh.ya2o.avionics.flightCrew.copilotName")

  override def receive: Receive = {
    case ReadyToGo =>
      context.parent ! GiveMeControl
      copilot = context.actorFor("../" + copilotName)
    //      autopilot = context.actorFor("../Autopilot")
    case Controls(controlSurfaces) =>
      controls = controlSurfaces

  }
}


class Copilot(plane: ActorRef, altimeter: ActorRef) extends Actor {

  import Pilot._
  import Plane._

  var controls: ActorRef = context.system.deadLetters
  var pilot: ActorRef = context.system.deadLetters
  //  var autopilot: ActorRef = context.system.deadLetters

  var pilotName = context.system.settings.config.getString("bzh.ya2o.avionics.flightCrew.pilotName")

  override def receive: Receive = {
    case ReadyToGo =>
      pilot = context.actorFor("../" + pilotName)
      context.watch(pilot)
    // autopilot = context.actorFor("../Autopilot")
    case Terminated(_) =>
      // pilot died
      plane ! GiveMeControl
  }
}

trait PilotProvider {
  def newPilot(plane: ActorRef, controls: ActorRef, altimeter: ActorRef): Actor = new Pilot(plane, controls, altimeter)

  def newCopilot(plane: ActorRef, controls: ActorRef, altimeter: ActorRef): Actor = new Copilot(plane, altimeter)

  //  def autopilot: Actor = new AutoPilot
}
