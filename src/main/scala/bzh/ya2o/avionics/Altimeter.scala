package bzh.ya2o.avionics

import akka.actor.{Props, Actor, ActorSystem, ActorLogging}
import scala.concurrent.duration._ // the duration package object extends Ints with some timing functionality

object Altimeter {
  def apply() = new Altimeter with ProductionEventSource

  case class RateChange(amount: Float)
  case class AltitudeUpdate(altitude: Double)
}

class Altimeter extends Actor with ActorLogging { this: EventSource =>
  import Altimeter._

  // we need an ExecutionContext for the scheduler.This actor's dispatcher can serve that purpose.
  // The scheduler's work will be dispatched on this actor's own dispatcher.
  implicit val ec = context.dispatcher

  val ceiling = 43000 // maximum ceiling in 'feet'
  val maxRateOfClimb = 5000 // maximum rate of climb in 'feet per minute'
  var rateOfClimb = 0f // varying rate of climb in 'feet per minute'
  var altitude = 0d // current altitude

  // as time passes, we need to change the altitude based on the time passed.
  var lastTick = System.currentTimeMillis()

  // we need to periodically update the altitude. This scheduled message tells when to do this.
  case object Tick

  val ticker = context.system.scheduler.schedule(100.millis, 100.millis, self, Tick)

  override def receive = eventSourceReceive orElse altimeterReceive

  private def altimeterReceive: Receive = {
    case RateChange(amount) =>
      // truncate the range to [-1,+1]
      rateOfClimb = amount.min(1.0f).max(-1.0f) * maxRateOfClimb
      log.info(s"Altimeter changed rate of climb to $rateOfClimb")
    case Tick =>
      val tick = System.currentTimeMillis()
      altitude = altitude + rateOfClimb * ((tick - lastTick) / 60000.0)
      lastTick = tick
      sendEvent(AltitudeUpdate(altitude))
  }

  override def postStop(): Unit = ticker.cancel()

}

trait AltimeterProvider{
  def newAltimeter: Actor = Altimeter()
}
