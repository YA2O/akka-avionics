import sbt._
import sbt.Keys._

object AvionicsBuild extends Build {

  lazy val avionics = Project(
    id = "avionics",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "Avionics",
      organization := "bzh.ya2o",
      version := "0.1-SNAPSHOT",
      scalaVersion := "2.10.0",
      scalacOptions ++= Seq("-feature", "-deprecation"),
      resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases",
      libraryDependencies ++= Seq(
        "com.typesafe.akka" %% "akka-actor" % "2.1.2",
        "com.typesafe.akka" %% "akka-testkit" % "2.1.2",
        "org.scalatest" % "scalatest_2.10" % "2.1.0" % "test"
      )
    )
  )
}
